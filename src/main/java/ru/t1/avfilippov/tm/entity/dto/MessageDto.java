package ru.t1.avfilippov.tm.entity.dto;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MessageDto {

    private String value;

    public MessageDto(String value) {
        this.value = value;
    }

}
