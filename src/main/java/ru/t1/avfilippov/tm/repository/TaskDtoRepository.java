package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;

import java.util.List;
import java.util.Optional;

public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {

    long countByUserId(final String userId);

    Optional<TaskDto> findByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

    List<TaskDto> findAllByUserId(String userId);

}
